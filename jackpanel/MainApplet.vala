/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;
using Panel;
using Jackpanel;


public static BonoboUI.Verb[] verbs;

public static JackpanelApplet panel;


public class MainApplet {
    public static bool panel_factory (Applet applet, string iid) {
        panel = new JackpanelApplet ();
        applet.add (panel);
        
        verbs = new BonoboUI.Verb[7];
        verbs[0].cname = "JackpanelAbout";
        verbs[0].cb = () => {
            panel.show_about_dialog ();
        };
        verbs[0].user_data = panel;
        
        verbs[1].cname = "JackpanelConfiguration";
        verbs[1].cb = () => {
            panel.show_configuration_dialog ();
        };
        verbs[1].user_data = panel;
        
        verbs[2].cname = "JackpanelStop";
        verbs[2].cb = () => {
            panel.stop_jack ();
        };
        verbs[2].user_data = panel;

        verbs[3].cname = "JackpanelStart";
        verbs[3].cb = () => {
            panel.start_jack ();
            panel.post_jack_init ();
        };
        verbs[3].user_data = panel;

        verbs[4].cname = "JackpanelRestart";
        verbs[4].cb = () => {
            panel.restart_jack ();
        };
        verbs[4].user_data = panel;

        verbs[5].cname = "JackpanelToggleDisplay";
        verbs[5].cb = () => {
            panel.toggle_transport_window ();
        };
        verbs[5].user_data = null;

        verbs[6].cname = null;
        verbs[6].cb = null;
        verbs[6].user_data = null;
                    
        applet.setup_menu ("""
<popup name="button3">
    <menuitem name="jackpanel About Item"          verb="JackpanelAbout"         _label="_About"        pixtype="stock" pixname="gtk-about"/>
    <menuitem name="jackpanel Configuration Item"  verb="JackpanelConfiguration" _label="_Preferences" pixtype="stock" pixname="gtk-properties"/>
    <menuitem name="jackpanel Stop Item"           verb="JackpanelStop"          _label="_Stop JACK" pixtype="stock" pixname="gtk-stop"/>
    <menuitem name="jackpanel Start Item"          verb="JackpanelStart"         _label="S_tart JACK" pixtype="stock" pixname="gtk-ok"/>
    <menuitem name="jackpanel Restart Item"        verb="JackpanelRestart"       _label="_Restart JACK" pixtype="stock" pixname="gtk-refresh"/>
    <menuitem name="jackpanel Toggle Display Item" verb="JackpanelToggleDisplay" _label="Toggle Transport _Display" pixtype="stock" pixname="gtk-media-play"/>
</popup>
""",
            verbs,
            panel);

        applet.show_all ();
        
        return true;
    }

    public static int main (string[] args) {
        Gnome.Program.init ("Jackpanel_Applet", "0", Gnome.libgnomeui_module,
                                          args, "sm-connect", false);
        return Applet.factory_main ("OAFIID:Jackpanel_Applet_Factory",
                                    typeof (Panel.Applet),
                                    MainApplet.panel_factory);
    }
}

