/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;

namespace Jackpanel {

public class HardwareInfo : Object {
    Hal.Context   ctx;
    DBus.RawError error;
    
    private HashTable<int, string> _capture_devices;
    private HashTable<int, string> _playback_devices;
    
    private List<string> result;
    
    public List<string> capture_devices {
        get {
            lock (result) {
                result = new List<string> ();
                for (int i = 0; i < _capture_devices.size (); i++) {
                    result.append (_capture_devices.lookup (i));
                }
            }
            return result;
        }
    }

    public List<string> playback_devices {
        get {
            lock (result) {
                result = new List<string> ();
                for (int i = 0; i < _playback_devices.size (); i++) {
                    result.append (_playback_devices.lookup (i));
                }
            }
            return result;
        }
    }

    construct {
        error = DBus.RawError ();
        
        ctx = new Hal.Context ();
        ctx.set_dbus_connection (DBus.RawBus.get (DBus.BusType.SYSTEM, ref error));
        ctx.init (ref error);
        
        enumerate_alsa_devices ();
    }

    public void enumerate_alsa_devices () {
        _capture_devices  = new HashTable<int, string> (direct_hash, direct_equal);
        _playback_devices = new HashTable<int, string> (direct_hash, direct_equal);

        var results = ctx.find_device_by_capability ("alsa", ref error);
        
        foreach (string udi in results) {
            string type = ctx.device_get_property_string (udi, "alsa.type", ref error);
            if (type == "sequencer") {
                debug ("got sequencer");
                continue;
            }
            
            if (type == "playback") {
                int card_nr    = ctx.device_get_property_int    (udi, "alsa.card",    ref error);
                int device_nr  = ctx.device_get_property_int    (udi, "alsa.device",  ref error);
                string card_id = ctx.device_get_property_string (udi, "alsa.card_id", ref error);
                if (true) {
                    _playback_devices.insert (card_nr, card_id);
                    debug ("got playback card: %d, device: %d with name %s", card_nr, device_nr, card_id);                
                } else {
                    debug("got playback card: %d, device: %d with name %s (not appended)", card_nr, device_nr, card_id);                
                }
            } else if (type == "capture") {
                int card_nr = ctx.device_get_property_int (udi, "alsa.card", ref error);
                int device_nr  = ctx.device_get_property_int    (udi, "alsa.device",  ref error);
                string card_id = ctx.device_get_property_string (udi, "alsa.card_id", ref error);
                if (true) {
                    _capture_devices.insert (card_nr, card_id);
                    debug ("got capture card: %d, device: %d with name %s", card_nr, device_nr, card_id);                
                } else {
                    debug("got capture card: %d, device: %d with name %s (not appended)", card_nr, device_nr, card_id);                
                }
            } else {
                debug ("unknown type: %s", type);
            }
        }
            
        return;
    }
}

} //namespace Jackpanel

