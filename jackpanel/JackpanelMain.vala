/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {
    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        window.set_default_icon_name ("jackpanel");
        var panel  = new Jackpanel (Path.get_basename(args[0]));
        window.add (panel);
        window.allow_grow = false;
        window.destroy += Gtk.main_quit;
        window.show_all ();
        Gtk.main ();
        
        if (panel.initialized) {
            return 0;
        } else {
            return 1;
        }
    }
} //namespace Jackpanel

