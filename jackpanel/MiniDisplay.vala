/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {

public class MiniDisplay : PanelDisplayBase {
    private Jack.Client? make_vala_include_jack_h_workaround;

    private DisplayState display_state = DisplayState.TIME;
    
    private bool _off;
    public bool off { 
        get {
            return _off;
        } 
        set {
            if (value != _off) {
                _off = value;
                queue_draw ();
            }
        }
    }
    
    public enum DisplayState {
        TIME = 0,
        TIME_ALT,
        ENUM_END
    }
    
    protected override void redraw_time () {
        if (display_state == DisplayState.TIME) queue_draw ();
    }
    
    protected override void redraw_time_alt () {
        if (display_state == DisplayState.TIME_ALT) queue_draw ();
    }
        
    construct {
        _width  = 128;
        _height = 25;
        time = "00:00:00";
        set_size_request (width, height);
        off = true;
    }
    
    protected override bool draw_contents (Cairo.Context cr, Gdk.EventExpose event) {
        double padding                  = 3;
        double font_size                = 12;
        double x                        = 0;
        double y                        = font_size - padding + 1.0;
        Cairo.TextExtents ext           = Cairo.TextExtents();
        
        // Realtime
        set_font (cr, font_size);

        if (off) {
            return true;
        }

        if (xruns > 0) {
            string xruns_str = "X-Runs: %u".printf (xruns);
            text (cr, xruns_str, padding + x, y, font_size, "#ff0000");
        } else {
            string rt = "RT";
            cr.text_extents (rt, out ext);
            if (realtime) {
                text (cr, rt, padding, y, font_size);
            }
            
            realtime_x = 0.0;
            realtime_y = y - ext.height;
            realtime_w = ext.x_advance + ext.x_bearing;
            realtime_h = ext.height;
            
            // buffer size
            string buffer_size_str = "%2.1fms".printf (buffer_size * 1000.0 / (double)sample_rate);
            
            cr.text_extents (buffer_size_str, out ext);
            var y_popup_triangle = y - ext.height + 2.0;
            
            buffer_size_x = realtime_x + realtime_w + 2 * padding;
            buffer_size_y = y_popup_triangle;
            buffer_size_w = ext.x_advance + ext.x_bearing + popup_triangle_width ();
            buffer_size_h = ext.height;
            text (cr, buffer_size_str, buffer_size_x, y, font_size);
            draw_popup_triangle (cr, buffer_size_x + ext.x_advance + ext.x_bearing + 1.0, y_popup_triangle);
            
            // sample_rate
            cr.text_extents (sample_rate_abbrev, out ext);        
            
            sample_rate_x = buffer_size_x + buffer_size_w + padding + padding;
            sample_rate_y = y_popup_triangle;
            sample_rate_w = ext.x_advance + ext.x_bearing + popup_triangle_width ();
            sample_rate_h = ext.height;
            text (cr, sample_rate_abbrev, sample_rate_x, y, font_size);
            draw_popup_triangle (cr, sample_rate_x + ext.x_advance + ext.x_bearing + 1.0, y_popup_triangle);
        }
        
        /*
        switch (display_state) {
            case DisplayState.TIME:
                // show time text
                text (cr, time, padding + x, y, font_size);
                break;
            case DisplayState.TIME_ALT: {
                    var adjusted_x = (time_alt.contains ("|")) ? x - 3.0 : x;
                    text (cr, time_alt, adjusted_x, y, font_size);
                }
                break;
            default:
                assert_not_reached ();
                break;
        }
        */
        
        return true;
    }
    
    public signal void double_click ();
    
    public override bool button_press_event (Gdk.EventButton event) {
        if (event.type == EventType.2BUTTON_PRESS && event.button == 1) {
            double_click ();
            return true;
        }

        if (event.type == EventType.BUTTON_PRESS && event.button == 1) {
            short disp_state = (short)display_state;
            if (off) return false;
            if (xruns > 0) {
                reset_xruns_requested ();
                xruns = 0;
            } else {
                return handle_events_for_active_items (event);
            }
            
            return true;
        } 
                
        return false;
    }
    
}

} //namespace Jackpanel

