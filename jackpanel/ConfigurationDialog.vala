/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {

public class ConfigurationDialog {
    private Jack.Client? make_vala_include_jack_h_workaround;

    public static const string[] drivers = {"alsa", "firefire", "net", "dummy"};
    public static const string[] dithers = {"none", "triangular", "rectangular", "shaped"};
    
    public enum SyncDirection {
        CONFIG_TO_DIALOG,
        DIALOG_TO_CONFIG
    }
    
    public static Builder builder;
    public static Configuration config;

    
    public static void sync_config_and_dialog (SyncDirection direction) {
        foreach (ParamSpec pspec in ((ObjectClass)typeof(Configuration).class_peek ()).list_properties ()) {
            debug ("getting paramspec: %s", pspec.name);
            var widget = builder.get_object (pspec.name) as Gtk.Widget;
            debug ("got widget: %x", (uint)widget);
            
            if (pspec is ParamSpecInt) {
                debug ("Got Int");
                KnobWithDisplay knob  = widget as KnobWithDisplay;
                Gtk.ComboBox    combo = widget as Gtk.ComboBox;
                Value val = Value (typeof(double));
                config.get_property (pspec.name, ref val);
                
                if (knob != null) {
                    if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                        KnobWithDisplay.DisplayFunc val_with_default = (knob, display, knob_with_display) => {
                            double knob_value = knob.get_value ();
                            if (knob_value == -1) {
                                display.text = "default";
                            } else {
                                display.text = knob_value.to_string () + " " + knob_with_display.unit;
                            }
                        };
                        
                        knob.set_display_func (val_with_default);
                        
                        debug("setting knob to %f", (int)val.get_double ());
                        knob.adjustment.value = val.get_double ();
                    } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                        val.set_double (knob.adjustment.value);
                        config.set_property (pspec.name, val);
                    }
                }
                
                if (combo != null) {
                    if (pspec.name == "sample-rate") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            for (int i = 0; i < PanelDisplayBase.sample_rates.length; i++) {
                                debug ("checking sample rate %d: %u", i, PanelDisplayBase.sample_rates[i]);
                                if (PanelDisplayBase.sample_rates[i] == (Jack.NFrames)val.get_double ()) {
                                    combo.active = i;
                                    debug ("found sample rate: %f", val.get_double ());
                                }
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                config.sample_rate = (int)PanelDisplayBase.sample_rates[combo.active];
                            }
                        }
                    }
                    
                    if (pspec.name == "buffer-size") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            for (int i = 0; i < PanelDisplayBase.buffer_sizes.length; i++) {
                                debug ("checking buffer size %d: %u", i, PanelDisplayBase.buffer_sizes[i]);
                                if (PanelDisplayBase.buffer_sizes[i] == (uint)val.get_double ()) {
                                    combo.active = i;
                                    debug ("found buffer size: %f", val.get_double ());
                                }
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                config.buffer_size = (int)PanelDisplayBase.buffer_sizes[combo.active];
                            }
                        }
                    }
                }
            }
            
            if (pspec is ParamSpecBoolean) {
                Gtk.ToggleButton button = widget as Gtk.ToggleButton;
                Value val = Value (typeof(bool));
                config.get_property (pspec.name, ref val);
                
                if (button != null) {
                    button.modify_bg (Gtk.StateType.ACTIVE, color_from_string (DisplayBase.text_color_default));
                    if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                        debug("setting button to %d", (int)val.get_boolean ());
                        button.set_active (val.get_boolean ());
                    } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                        val.set_boolean (button.get_active ());
                        config.set_property (pspec.name, val);
                    }
                }
            }
            
            if (pspec.value_type == typeof(string)) {
                debug ("Got String");
                Value val = Value (typeof(string));
                config.get_property (pspec.name, ref val);
                Gtk.ComboBox combo = widget as Gtk.ComboBox;
                if (combo != null) {
                    debug ("got combo");
                    if (pspec.name == "driver") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            for (int i = 0; i < drivers.length; i++) {
                                debug ("checking driver %d: %s", i, drivers[i]);
                                if (drivers[i] == val.get_string()) {
                                    combo.active = i;
                                    debug ("found driver: %s", val.get_string());
                                }
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                config.driver = drivers[combo.active];
                            }
                        }
                    }
                    
                    if (pspec.name == "input-device") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            var parts = val.get_string ().split (":");
                            if (parts.length == 2) {
                                assert (parts[0] == "hw");
                                combo.active = parts[1].to_int ();
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                config.input_device = "hw:%d".printf(combo.active);
                            }
                        }
                    }
                    
                    if (pspec.name == "output-device") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            var parts = val.get_string ().split (":");
                            if (parts.length == 2) {
                                assert (parts[0] == "hw");
                                combo.active = parts[1].to_int ();
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                config.output_device = "hw:%d".printf(combo.active);
                            }
                        }
                    }
                    

                    if (pspec.name == "dither") {
                        if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                            debug ("looking for dither: %s", val.get_string());
                            for (int i = 0; i < dithers.length; i++) {
                                if (dithers[i] == val.get_string()) {
                                    combo.active = i;
                                    debug ("found dither: %s", val.get_string());
                                }
                                if ("" == val.get_string ()) {
                                    combo.active = 0;
                                }
                            }
                        } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                            if (combo.active >= 0) {
                                val.set_string (dithers[combo.active]);
                                config.set_property (pspec.name, val);
                            }
                        }
                    }
                }
                
                Gtk.Entry entry = widget as Gtk.Entry;
                if (entry != null) {
                    if (direction == SyncDirection.CONFIG_TO_DIALOG) {
                        debug ("got entry");
                        entry.set_text (val.get_string ());
                    } else if (direction == SyncDirection.DIALOG_TO_CONFIG) {
                        val.set_string (entry.get_text ());
                        config.set_property (pspec.name, val);
                    }
                }
            }
        } // end for    
    }
    
    static bool result;
    public static bool show_configuration_dialog () {
        result = false;
        try {
            builder = new Builder ();
            builder.add_from_file (configuration_dialog_xml);
            var dialog = builder.get_object ("configuration-dialog") as Gtk.Dialog;
            assert (dialog != null);
            config = new Configuration ();
            if (!config.exists ()) {
                debug ("Config not valid, initializing empty\n");
                config.init ();
            } else {
                config.config_root = "default";
            }
            assert (config != null);
            
            var hwinfo = new HardwareInfo ();
            var input_combo  = builder.get_object ("input-device") as Gtk.ComboBox;
            var input_store = input_combo.get_model () as Gtk.ListStore;
            input_store.clear ();
            Gtk.TreeIter iter = Gtk.TreeIter ();
            foreach (string device in hwinfo.capture_devices) {
                input_store.append (out iter);
                var val = Value (typeof(string));
                val.set_string (device);
                input_store.set_value (iter, 0, val);
            }
                
            var output_combo  = builder.get_object ("output-device") as Gtk.ComboBox;
            var output_store = output_combo.get_model () as Gtk.ListStore;
            output_store.clear ();
            foreach (string device in hwinfo.playback_devices) {
                output_store.append (out iter);
                var val = Value (typeof(string));
                val.set_string (device);
                output_store.set_value (iter, 0, val);
            }
            
            //Log.set_handler (null, LogLevelFlags.LEVEL_DEBUG, () => {});
            sync_config_and_dialog (SyncDirection.CONFIG_TO_DIALOG);
            
            dialog.show_all ();
            dialog.response += (dialog, response) => {
                debug ("dialog answered: %d", response);
                if (response == Gtk.ResponseType.OK) {
                    debug ("dialog OK");
                    sync_config_and_dialog (SyncDirection.DIALOG_TO_CONFIG);
                    result = true;
                }
            };
            dialog.run ();
            dialog.destroy ();
            config = null;
        } catch (Error e) {
            var msg = new MessageDialog (null, DialogFlags.MODAL,
                                         MessageType.ERROR, ButtonsType.CANCEL, 
                                         "Failed to load UI\n%s", e.message);
            msg.run ();
            msg.destroy ();
        }
        return result;
    }
}
} //namespace Jackpanel

