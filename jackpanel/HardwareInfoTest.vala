/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;

namespace Jackpanel {

public class HalTest {
    public static void alsa_test () {
        var hwinfo = new HardwareInfo ();
        
        foreach (string d in hwinfo.playback_devices)
            stdout.printf ("Got playback device: %s\n", d);
        foreach (string d in hwinfo.capture_devices)
            stdout.printf ("Got capture device: %s\n", d);
            
        return;
    }
    
    
    static int main (string[] args) {
        Test.init (ref args);
        Test.add_func ("/jackpanel/configuration/alsa", alsa_test);
        Test.run ();
        
        return 0;
    }
}

} //namespace Jackpanel

