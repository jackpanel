/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;
using Jack;

namespace Jackpanel {

public class Jackpanel : JackpanelBase {    
    // widgets
    Display  display;
    TransportButton    to_start;
    TransportButton    fast_backward;
    TransportButton    play;
    TransportButton    stop;
    TransportButton    fast_forward;
    
    float           acceleration = 1.0f;
    
    protected override void on_transport_stopped () {
        if (play.active) {
            play.set_active_no_signals (false);
        }
    }
    
    protected override void on_transport_rolling () {
        if (!play.active) {
            play.set_active_no_signals (true);
        }
    }
      
   private bool show_state () {
        if (client == null || !initialized) {
            return false;
        }
        
        display.realtime = client.is_realtime(); 
        
        display.sample_rate = client.get_sample_rate ();
        
        display.buffer_size = client.get_buffer_size ();
        
        display.xruns = no_xruns;
        
        if (display.xruns != no_xruns) display.xruns = no_xruns;
        
        // this one redraws the panel
        show_time (display); 
        
        return true;
    }

    public Jackpanel (string client_name) {
        this.client_name = client_name;
        
        var table            = new Table (5, 2, false);
        display              = new Display ();
        
        display.sample_rate_changed += (display, new_rate) => {
            config.sample_rate = (int)new_rate;
            restart_jack ();
        };
        
        display.buffer_size_changed += (display, new_size) => {
            client.set_buffer_size (new_size);
            display.buffer_size = new_size;
            config.buffer_size = (int)new_size;
        };
        
        display.toggle_realtime_requested += (display, should_be_realtime) => {
            if (should_be_realtime != config.realtime) {
                config.realtime = should_be_realtime;
                restart_jack ();
            }
        };
        
        display.preferences += (display) => {
            show_configuration_dialog ();
        };
        
        display.reset_xruns_requested += (display) => {
            no_xruns = 0;
        };
        
        to_start = new TransportButton ();
        to_start.icon_type = TransportButton.IconType.TO_START;
        to_start.set_size_request (31, 20);
        to_start.pressed += () => {
            client.transport_locate(0);
        };

        fast_backward = new TransportButton ();
        fast_backward.icon_type = TransportButton.IconType.FAST_BACKWARD;
        fast_backward.set_size_request (31, 20);
        fast_backward.pressed  += ()  => {
            wind_transport (-1.0f);

            Timeout.add (200, () => {
                accelerate_transport (fast_backward, -1.0f);
                return true;
            });
        };
        
        fast_backward.released += () => {
            acceleration = 1.0f;
        };

        play = new TransportButton ();
        play.icon_type = TransportButton.IconType.PLAY;
        play.set_size_request (31, 20);
        play.toggled += (button, active) => {
            if (active) {
                client.transport_start ();
            } else {
                client.transport_stop ();
            }
        };

        stop = new TransportButton ();
        stop.icon_type = TransportButton.IconType.STOP;
        stop.set_size_request (31, 20);
        stop.pressed += () => {
            client.transport_stop ();
            play.active = false;
        };

        fast_forward = new TransportButton ();
        fast_forward.icon_type = TransportButton.IconType.FAST_FORWARD;
        fast_forward.set_size_request (31, 20);
        fast_forward.pressed += ()  => {
            wind_transport (1.0f);

            Timeout.add (200, () => {
                accelerate_transport (fast_forward, 1.0f);
                return true;
            });
        };
        
        fast_forward.released += () => {
            acceleration = 1.0f;
        };
        
        
        table.attach_defaults (display,       0, 5, 0, 1);

        table.attach_defaults (to_start,      0, 1, 1, 2);
        table.attach_defaults (fast_backward, 1, 2, 1, 2);
        table.attach_defaults (fast_forward,  2, 3, 1, 2);
        table.attach_defaults (stop,          3, 4, 1, 2);
        table.attach_defaults (play,          4, 5, 1, 2);

        table.set_col_spacing (0, 4);
        table.set_col_spacing (1, 4);
        table.set_col_spacing (2, 10);
        table.set_col_spacing (3, 4);
        add (table);
        
        post_jack_init += () => {
            debug ("post_jack_init: in handler");
            if (initialized) {
                Timeout.add (100, show_state);
                to_start.sensitive = true;
                fast_backward.sensitive = true;
                fast_forward.sensitive = true;
                stop.sensitive = true;
                play.sensitive = true;
                Thread.usleep (120000);
                display.jack_running = true;
                display.queue_draw ();
            } else {
                to_start.sensitive = false;
                fast_backward.sensitive = false;
                fast_forward.sensitive = false;
                stop.sensitive = false;
                play.sensitive = false;
                display.jack_running = false;
                display.time     = "-- : -- : --";
                display.time_alt = "----------";
            }
        };
        
        if (!initialized && config.start_jack) {
            start_jack ();
            post_jack_init ();
            
            if (!initialized) {
                init_error_dialog ();
            }
        } else {
            post_jack_init ();
        }
    }
    
    ~Jackpanel () {
        stop_jack ();
    }
    
    private void wind_transport (float direction) {
        Position pos;
        client.transport_query(out pos);
        
        float location   = (((float) pos.frame / pos.frame_rate) + direction * acceleration) * pos.frame_rate;
        
        if (location < 0.0f) {
            location = 0.0f;
        }
        
        client.transport_locate((NFrames)location);
    }
    
    private bool accelerate_transport (TransportButton button, float direction) {
        if (button.active) {
            if (acceleration < 120.0f) {
                acceleration *= 1.11f;
            }
            
            wind_transport (direction);
            return true;
        }
        return false;    
    }
}

} //namespace Jackpanel

