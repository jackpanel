/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;
using Panel;

namespace Jackpanel {

public class JackpanelApplet : JackpanelBase {
    private MiniDisplay display;
    private Jackpanel   panel;
    private new Gtk.Window  window;

    private bool show_state () {
        if ((!initialized) || client == null) {
            display.off = true;
            return false;
        }
        
        display.realtime = client.is_realtime();
        
        display.sample_rate = client.get_sample_rate ();
        
        display.buffer_size = client.get_buffer_size ();
        
        if (display.xruns != no_xruns) display.xruns = no_xruns;
        
        display.off = false;
        
        // this one redraws the panel
        show_time (display); 
        
        return true;
    }
    
    protected override void on_transport_stopped () {}
    
    protected override void on_transport_rolling () {}

    protected void post_jack_init_function () {
        debug ("PanelApplet: post_jack_init");
        if (initialized) {
            Timeout.add (100, show_state);
        } else {
            display.off = true;
        }
    }

    construct {
        client_name = "jackpanel-applet";
        
        display = new MiniDisplay ();
        
        display.sample_rate_changed += (display, new_rate) => {
            config.sample_rate = (int)new_rate;
            restart_jack ();
        };
        
        display.buffer_size_changed += (display, new_size) => {
            client.set_buffer_size (new_size);
            display.buffer_size = new_size;
            config.buffer_size = (int)new_size;
        };
        
        display.toggle_realtime_requested += (display, should_be_realtime) => {
            if (should_be_realtime != config.realtime) {
                config.realtime = should_be_realtime;
                restart_jack ();
            }
        };
        
        display.reset_xruns_requested += (display) => {
            no_xruns = 0;
        };
        add (display);
        
        window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        window.set_default_icon_name ("jackpanel");
        // JACK gets started here
        panel  = new Jackpanel ("jackpanel-applet");
        panel.post_jack_init += post_jack_init_function;
        window.add (panel);
        window.set_deletable (false);
        window.allow_grow = false;
        window.show_all ();
        window.visible = false;
        
        post_jack_init += () => {
            panel.post_jack_init ();
        };
        
        display.double_click += () => {
            window.visible = !window.visible;
        };

        post_jack_init_function ();
    }
    
    public void toggle_transport_window () {
        display.double_click ();
    }
    
    ~JackPanelApplet () {
        stop_jack ();
    }
}

} //namespace Jackpanel
