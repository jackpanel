/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {

    static int main (string[] args) {
        Gtk.init (ref args);
        var window = new Gtk.Window (Gtk.WindowType.TOPLEVEL);
        window.set_default_icon_name ("jackpanel");
        var display  = new MiniDisplay ();
        window.add (display);
        window.destroy += Gtk.main_quit;
        window.show_all ();
        
        Gtk.main ();
        return 0;
    }

} //namespace Jackpanel

