/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;

namespace Jackpanel {

public class Configuration : GLib.Object {
    GConf.Client gconf;
    
    private static string jackpanel_root = "/apps/jackpanel";
    public  static string root ()          { return jackpanel_root; }
    public void   set_test_mode () { jackpanel_root = "/apps/jackpanel-test"; }
    
    public Configuration () {
        gconf = GConf.Client.get_default ();
    }
    
    public void init () {
        config_root       = "default";
        server_command    = "";
        server_name       = "";
        driver            = "";
        input_device      = "";
        output_device     = "";
        midi_driver       = "";
        dither            = "";
        realtime          = false;
        soft_mode         = false;
        force_16_bit      = false;
        hw_monitoring     = false;
        hw_metering       = false;
        monitor_ports     = false;
        sample_rate       = -1;
        buffer_size       = -1;
        periods           = -1;
        realtime_priority = -1;
        port_max          = -1;
        inchannels        = -1;
        outchannels       = -1;
        client_timeout    = -1;
        
        jack_options      = "";
        driver_options    = "";
    }
    
    public bool exists () {
        return gconf.dir_exists (root ()) && server_command != "";
    }
    
    public static bool nonempty (string str) { return str != null && str != "";}
    
    public void sweep () {
        if (exists ()) {
            gconf.recursive_unset (root (), GConf.UnsetFlags.NAMES);
            init ();
        }
    }
    
    private string _config_root;
    public string config_root { 
        get { return  _config_root; } 
        set {
            _config_root = root () + "/configs/";
            if ("/" in value) {
                var parts = value.split ("/");
                active_config = parts[parts.length - 1];
            } else {
                active_config = value;
            }
            _config_root += active_config;
        }
    }
    
    void append_if_nonnegative (StringBuilder b, string option, int val) {
        if (val >= 0) b.append (" %s %d".printf (option, val));
    }

    void append_if_nonempty (StringBuilder b, string option, string? val) {
        if (val != null && val != "") b.append (" %s %s".printf (option, val));
    }
    
    public string to_command_line () {
        var b = new StringBuilder ();
        b.append (server_command);
        append_if_nonempty (b, " --name", server_name);
        if (realtime) 
            b.append (" --realtime");
        else
            b.append (" --no-realtime");
            
        append_if_nonnegative (b, "--realtime-priority", realtime_priority);
        append_if_nonnegative (b, "--port-max", port_max);
        append_if_nonnegative (b, "--timeout", client_timeout);

        append_if_nonempty (b, "", jack_options);
        
        append_if_nonempty (b, "--driver", driver);
        
        // common driver options
        append_if_nonnegative (b, "--rate",        sample_rate);
        append_if_nonnegative (b, "--period",      buffer_size);
        
        if (driver != "dummy") {
            append_if_nonnegative (b, "--nperiods",    periods);
            append_if_nonnegative (b, "--inchannels",  inchannels);
            append_if_nonnegative (b, "--outchannels", outchannels);
        } else {
            append_if_nonnegative (b, "--capture",  inchannels);
            append_if_nonnegative (b, "--playback", outchannels);
        }
            
        if (driver == "alsa") {
            append_if_nonempty    (b, "--capture",  input_device);
            append_if_nonempty    (b, "--playback", output_device);
            append_if_nonempty    (b, "--midi",     midi_driver);
            append_if_nonempty    (b, "--dither",   dither);
            if (soft_mode)         b.append (" --softmode");
            if (force_16_bit)      b.append (" --shorts");
            if (hw_monitoring)     b.append (" --hwmon");
            if (hw_metering)       b.append (" --hwmeter");
            if (monitor_ports)     b.append (" --monitor");
        }
        
        append_if_nonempty (b, "", driver_options);
        
        return b.str;
    }
    
    private delegate void SetterDelegateString (string str);
    private delegate void SetterDelegateBool   (bool val);
    
    private void set_value (string[] prefixes, string part, SetterDelegateString set_value) {
        foreach (string prefix in prefixes) {
            if (part.has_prefix (prefix)) {
                debug ("check string %s for prefix %s", part, prefix);
                string val = part.replace (prefix, "");
                set_value (val);
                part_recognized = true;
                debug ("match: got %s", val);
            }
        }
    }
    
    private void set_bool (string[] prefixes, string part, SetterDelegateBool set_value)  {
        foreach (string prefix in prefixes) {
            if (part == prefix) {
                set_value (true);
                debug ("match: got true");
                part_recognized = true;
            }
        }
    }
    
    private bool after_driver;
    private bool part_recognized;
    public void from_command_line (string commandline) {
        init ();

        debug ("got commandline: %s", commandline);
        Regex args_with_spaces = new Regex ("(--?[A-Za-z]+)[ \t]+([^- \t]+)");
        string normalized = commandline.strip();
        normalized = args_with_spaces.replace (normalized, -1, 0, "\\1\\2"); 
        debug ("commandline (normalized): %s", normalized);
    
        after_driver = false;
        string[] parts = normalized.split (" ");
        debug ("config-root: %s, number of parts: %d", config_root, parts.length);
        server_command = parts[0];
        
        for (int i = 1; i < parts.length; i++) {
            string part = parts[i];
            part_recognized = false;
            debug ("analyzing part: %s", part);
            set_bool ({"-R", "--realtime"}, part, (on) => { realtime = on; });
            
            if (!after_driver) {
                set_value ({"-n", "--name"},              part, (str) => { server_name = str;                 });
                set_value ({"-P", "--realtime-priority"}, part, (str) => { realtime_priority = str.to_int (); });
                set_value ({"-d", "--driver"},            part, (str) => { driver = str; after_driver = true; });
                set_value ({"-p", "--port-max"},          part, (str) => { port_max = str.to_int ();          });
                set_value ({"-t", "--timeout"},           part, (str) => { client_timeout = str.to_int ();    });
                
                // catch all
                if (!part_recognized) {
                    debug ("jack options catch all: '%s'", part);
                    jack_options = (jack_options + " " + part).strip ();
                }
            }
            if (after_driver) {
                // common among backends
                set_value ({"-r", "--rate"},        part, (str) => { sample_rate   = str.to_int (); });
                set_value ({"-p", "--period"},      part, (str) => { buffer_size   = str.to_int (); });
                set_value ({"-n", "--nperiods"},    part, (str) => { periods       = str.to_int (); });
                set_value ({"-i", "--inchannels"},  part, (str) => { inchannels    = str.to_int (); });
                set_value ({"-o", "--outchannels"}, part, (str) => { outchannels   = str.to_int (); });
                
                if (driver == "alsa") {
                    set_value ({"-C", "--capture"},  part, (str) => { input_device  = str;           });
                    set_value ({"-P", "--playback"}, part, (str) => { output_device = str;           });
                    set_value ({"-X", "--midi"},     part, (str) => { midi_driver   = str;           });
                    set_value ({"-z", "--dither"},   part, (str) => { dither        = str;           });
                    set_bool  ({"-s", "--softmode"}, part, (on)  => { soft_mode     = on;            });
                    set_bool  ({"-S", "--shorts"},   part, (on)  => { force_16_bit  = on;            });
                    set_bool  ({"-H", "--hwmon"},    part, (on)  => { hw_monitoring = on;            });
                    set_bool  ({"-M", "--hwmeter"},  part, (on)  => { hw_metering   = on;            });
                    set_bool  ({"-m", "--monitor"},  part, (on)  => { monitor_ports = on;            });
                }
                
                if (driver == "net") {
                    // TODO - when catch-all isn't enough
                }
                
                if (driver == "freebob") {
                    // TODO - when catch-all isn't enough
                }
                
                // catch all
                if (!part_recognized) {
                    debug ("driver options catch all: '%s'", part);
                    driver_options = (driver_options + " " + part).strip ();
                }
                part_recognized = false;
            }
        }
    }

    protected string active_config {
        owned get { return gconf.get_string (root () + "/active_config"); }
        set { gconf.set_string (root () + "/active_config", value); }
    }
    
    public bool start_jack {
        get { return gconf.get_bool (root () + "/start_jack"); }
        set { gconf.set_bool (root () + "/start_jack", value); }    
    }

    public string server_command {
        owned get { return gconf.get_string (config_root + "/server_command"); }
        set { gconf.set_string (config_root + "/server_command", value); }
    }

    public string server_name {
        owned get { return gconf.get_string (config_root + "/server_name"); }
        set { gconf.set_string (config_root + "/server_name", value); }
    }

    public bool realtime {
        get { return gconf.get_bool (config_root + "/realtime"); }
        set { gconf.set_bool (config_root + "/realtime", value); }
    }

    public int realtime_priority {
        get { return gconf.get_int (config_root + "/realtime_priority"); }
        set { gconf.set_int (config_root + "/realtime_priority", value); }
    }
    
    public int port_max {
        get { return gconf.get_int (config_root + "/port_max"); }
        set { gconf.set_int (config_root + "/port_max", value); }
    }

    public int client_timeout {
        get { return gconf.get_int (config_root + "/client_timeout"); }
        set { gconf.set_int (config_root + "/client_timeout", value); }
    }

    public string driver {
        owned get { return gconf.get_string (config_root + "/driver"); }
        set { gconf.set_string (config_root + "/driver", value); }
    }

    public string midi_driver {
        owned get { return gconf.get_string (config_root + "/midi_driver"); }
        set { gconf.set_string (config_root + "/midi_driver", value); }
    }

    public int sample_rate {
        get { return gconf.get_int (config_root + "/sample_rate"); }
        set { gconf.set_int (config_root + "/sample_rate", value); }
    }

    public int buffer_size {
        get { return gconf.get_int (config_root + "/buffer_size"); }
        set { gconf.set_int (config_root + "/buffer_size", value); }
    }

    public int periods {
        get { return gconf.get_int (config_root + "/periods"); }
        set { gconf.set_int (config_root + "/periods", value); }
    }

    public bool soft_mode {
        get { return gconf.get_bool (config_root + "/soft_mode"); }
        set { gconf.set_bool (config_root + "/soft_mode", value); }
    }

    public bool force_16_bit {
        get { return gconf.get_bool (config_root + "/force_16_bit"); }
        set { gconf.set_bool (config_root + "/force_16_bit", value); }
    }
    
    public string dither {
        owned get { return gconf.get_string (config_root + "/dither"); }
        set { gconf.set_string (config_root + "/dither", value); }
    }

    public bool hw_monitoring {
        get { return gconf.get_bool (config_root + "/hw_monitoring"); }
        set { gconf.set_bool (config_root + "/hw_monitoring", value); }
    }

    public bool hw_metering {
        get { return gconf.get_bool (config_root + "/hw_metering"); }
        set { gconf.set_bool (config_root + "/hw_metering", value); }
    }

    public bool monitor_ports {
        get { return gconf.get_bool (config_root + "/monitor_ports"); }
        set { gconf.set_bool (config_root + "/monitor_ports", value); }
    }

    public string input_device {
        owned get { return gconf.get_string (config_root + "/input_device"); }
        set { gconf.set_string (config_root + "/input_device", value); }
    }

    public string output_device {
        owned get { return gconf.get_string (config_root + "/output_device"); }
        set { gconf.set_string (config_root + "/output_device", value); }
    }

    public int inchannels {
        get { return gconf.get_int (config_root + "/inchannels"); }
        set { gconf.set_int (config_root + "/inchannels", value); }
    }

    public int outchannels {
        get { return gconf.get_int (config_root + "/outchannels"); }
        set { gconf.set_int (config_root + "/outchannels", value); }
    }

    public string jack_options {
        owned get { return gconf.get_string (config_root + "/jack_options"); }
        set { gconf.set_string (config_root + "/jack_options", value); }
    }

    public string driver_options {
        owned get { return gconf.get_string (config_root + "/driver_options"); }
        set { gconf.set_string (config_root + "/driver_options", value); }
    }

}

} //namespace Jackpanel

