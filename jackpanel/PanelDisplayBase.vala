/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {

public abstract class PanelDisplayBase : DisplayBase {
    private Jack.Client? make_vala_include_jack_h_workaround;

    private string _time;
    public  string time { 
        get { return _time; } 
        set { 
            if (_time != value) {
                _time = value; 
                redraw_time (); 
            }
        } 
    }
    
    private string _time_alt;
    public string time_alt { 
        get { return _time_alt; } 
        set { 
            if (_time_alt != value) {
                _time_alt = value; 
                redraw_time_alt ();
            }
        } 
    }
    
    protected double xruns_x;
    protected double xruns_y;
    protected double xruns_w;
    protected double xruns_h;
    protected uint32 _xruns;
    
    public signal void reset_xruns_requested ();
    public uint32 xruns { 
        get {
            return _xruns;
        }
        set {
            if (_xruns != value) {
                _xruns = value;
                queue_draw ();
            }
        }
    }
    
    protected double realtime_x;
    protected double realtime_y;
    protected double realtime_w;
    protected double realtime_h;
    
    public signal void toggle_realtime_requested (bool new_state);    
    private bool _realtime;
    public  bool realtime { 
        get { return _realtime; } 
        set { 
            if (_realtime != value) {
                _realtime = value;
                queue_draw ();
            }
        } 
    }
    
    public static const Jack.NFrames[] sample_rates        = { 22050, 32000,   44100, 48000, 96000, 192000 };
    public static const string[]       sample_rates_abbrev = { "22k", "32k", "44.1k", "48k", "96k", "192k" };
    private uint8 _sample_rate_index;
    public  Jack.NFrames sample_rate { 
        get {
            return sample_rates[_sample_rate_index];
        }
        set {
            if (sample_rates[_sample_rate_index] != value) {
                for (uint8 i = 0; i < sample_rates.length; i++) {
                    if (sample_rates[i] == value) {
                        _sample_rate_index = i;
                    }
                }
                queue_draw ();
            }
        } 
    }
    
    public string sample_rate_abbrev {
        get {
            return sample_rates_abbrev[_sample_rate_index];
        }
    }
    
    public  signal void sample_rate_changed (Jack.NFrames new_sample_rate);
    protected double sample_rate_x;
    protected double sample_rate_y;
    protected double sample_rate_w;
    protected double sample_rate_h;
    protected Menu   sample_rate_menu;
    
    protected Jack.NFrames _buffer_size;
    public  Jack.NFrames buffer_size { 
        get {
            return _buffer_size;
        }
        set {
            if (_buffer_size != value) {
                _buffer_size = value;
                queue_draw ();
            }
        } 
    }
    
    public static const uint[] buffer_sizes = { 16, 32, 64, 128, 256, 512, 1024, 2048 };
    public  signal void buffer_size_changed (Jack.NFrames new_buffer_size);
    protected double buffer_size_x;
    protected double buffer_size_y;
    protected double buffer_size_w;
    protected double buffer_size_h;
    protected Menu   buffer_size_menu;
    
    protected abstract void redraw_time ();
    protected abstract void redraw_time_alt ();
    
    protected static double popup_triangle_width () { return 7.0; }
    
    public static void draw_popup_triangle (Cairo.Context cr, double x, double y) {
        cr.save ();
        set_source_color_string (cr, "#b3b6af");
        cr.new_path ();
        cr.move_to (x, y);
        cr.line_to (x + popup_triangle_width (), y);
        cr.line_to (x + popup_triangle_width () / 2.0, y + 4);
        cr.close_path ();
        cr.fill ();
        cr.restore ();
    }
    
    protected static void debug_rect (Cairo.Context cr, double x, double y, double w, double h) {
        cr.save ();
        set_source_color_string (cr, "#ff0000");
        cr.rectangle (x, y, w, h);
        cr.stroke ();
        cr.restore ();
    }
    
    protected bool is_in_rect (double x, double y, double r_x, double r_y, double r_w, double r_h) {
        x -= inner_x;
        y -= inner_y;
        return    (r_x <= x) && (x <= r_x + r_w)
               && (r_y <= y) && (y <= r_y + r_h);
    }
    
    protected bool handle_events_for_active_items (Gdk.EventButton event) {
        if (event.type == EventType.BUTTON_PRESS) {
            if (is_in_rect (event.x, event.y, sample_rate_x, sample_rate_y, sample_rate_w, sample_rate_h)) {
                sample_rate_menu = new Menu();
                for (int i = 0; i < sample_rates.length; i++) {
                    var item = new MenuItem.with_label ("%u".printf (sample_rates[i]));
                    item.activate += (item) => {
                        string rate_str = ((Gtk.Label)((Gtk.Item)item).get_child ()).get_text();
                        var new_rate = (Jack.NFrames) rate_str.to_ulong ();
                        if (new_rate != sample_rate) {
                            sample_rate_changed (new_rate);
                        }
                    };
                    sample_rate_menu.append (item);
                    
                }
                sample_rate_menu.popup (null, null, null, 3, event.time);
                sample_rate_menu.show_all();
                return true;
            }
            
            if (is_in_rect (event.x, event.y, buffer_size_x, buffer_size_y, buffer_size_w, buffer_size_h)) {
                buffer_size_menu = new Menu ();
                for (int i = 0; i < buffer_sizes.length; i++) {
                    var item = new MenuItem.with_label ("%2.1fms (%u samples)".printf (buffer_sizes[i]*1000.0/(double)sample_rate, buffer_sizes[i]));
                    item.user_data = &buffer_sizes[i];
                    item.activate += (item) => {
                        buffer_size_changed (*((Jack.NFrames *)item.user_data));
                    };
                    buffer_size_menu.append (item);
                }

                buffer_size_menu.popup (null, null, null, 3, event.time);
                buffer_size_menu.show_all();
                return true;
            }
        }

        if (event.type == EventType.BUTTON_PRESS && event.button == 1) {
            if (is_in_rect (event.x, event.y, realtime_x, realtime_y, realtime_w, realtime_h)) {
                toggle_realtime_requested (!realtime);
            }
            return true;
        }
        
        return false;
    }
    
}

} //namespace Jackpanel

