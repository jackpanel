/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;
using Jack;

namespace Jackpanel {

public static uint32 no_xruns;

public abstract class JackpanelBase : Alignment {

    protected int jackpanel_xrun_callback () {
        no_xruns++;
        return 0;
    }

    // state
    protected static Configuration   config;
    protected static string          client_name;
    protected static Client?         client = null;
    protected static Jack.Status     status;
    protected        string          time_in_frames;
    protected        string          time_in_bbt;
    protected        string          time_h_min_sec;
    
    protected static Pid             jackd_pid = (Pid) 0;

    protected static int             jack_stdout;
    protected static IOChannel       jack_output;
    protected static uint            jack_output_source = 0;
    protected static StringBuilder   jack_out = new StringBuilder ();
    protected static Mutex           jack_out_lock = new Mutex ();
    protected static uint            output_timeout;

    protected static int             jack_stderr;
    protected static IOChannel       jack_errors;
    protected static uint            jack_errors_source = 0;
    protected static StringBuilder   jack_err = new StringBuilder ();
    protected static Mutex           jack_err_lock = new Mutex ();
    protected static uint            error_timeout;
    
    construct {
        config = new Configuration ();
        if (!config.exists ()) {
            debug ("Config not valid, initializing empty\n");
            config.init ();
        } else {
            config.config_root = "default";
        }
        initialized = false;
    }
    
    public static bool initialized;
    
    public bool restart_jack () {
        debug ("restart_jack called from Thread: %u", (uint)Thread.self ());
        stop_jack ();
        Timeout.add (1000, () => {
            start_jack ();
            post_jack_init ();
            return false;
        });
        Thread.usleep (1100000);
        return initialized;
    }
    
    protected bool start_jack_client () {
        debug ("start_jack_client ()");
        if (client == null) {
            client = Client.open (client_name, Options.NoStartServer, out status);
        }
        
        if (client == null) {
            critical ("jack_client_open() failed, status = 0x%2.0x\n", status);
            if ((status & Jack.Status.ServerFailed) != 0) {
                critical ("Unable to connect to JACK server\n");
            }
            return false;
        } else {
            return true;
        }
    }
        
    private void reset_jack_output_sources () {
        Source.remove (jack_output_source);
        jack_output_source = 0;
        
        Source.remove (jack_errors_source);
        jack_errors_source = 0;
        
        Source.remove (output_timeout);
        output_timeout = 0;
        
        Source.remove (error_timeout);
        error_timeout = 0;        
        
        jack_output = null;
        jack_errors = null;
    }
        
    public void stop_jack () {
        debug ("stop_jack called from Thread: %u", (uint)Thread.self ());
        if (client != null) {
            client.deactivate ();
            client.close ();
            client = null;
        }

        reset_jack_output_sources ();
        
        debug ("jackd pid before restart: %d", (int)jackd_pid);
        
        Posix.pid_t pid_to_kill = (Posix.pid_t)0;
        if (jackd_pid != (GLib.Pid)0) {
            pid_to_kill = (Posix.pid_t)jackd_pid;
        } else {
            string jackd_pid_str;
            Process.spawn_sync (null,  {"/bin/pidof", "jackd"}, {}, 0, null, out jackd_pid_str);
            if ("" != jackd_pid_str) {
                pid_to_kill = (Posix.pid_t)jackd_pid_str.to_int ();
            }
        }
        
        if (pid_to_kill != (Posix.pid_t)0) {
            debug ("Killing PID: %d", (int)pid_to_kill);
            Posix.kill (pid_to_kill, Posix.SIGTERM);
            jackd_pid = (GLib.Pid)0;
            initialized = false;
        }
    }
    
    protected bool start_jack_server () {
        string cmdline;
        
        if (!config.exists ()) {
            string jackdrc_filename = Environment.get_home_dir () + Path.DIR_SEPARATOR_S + ".jackdrc";
            if (FileUtils.test (jackdrc_filename, FileTest.EXISTS) && 
                FileUtils.test (jackdrc_filename, FileTest.IS_REGULAR)) {
                message ("importing settings from ~/.jackdrc\n");
                string jackdrc;
                FileUtils.get_contents (jackdrc_filename, out jackdrc);
                config.from_command_line (jackdrc);
                
                var d = new Gtk.MessageDialog (null, Gtk.DialogFlags.MODAL, Gtk.MessageType.INFO, Gtk.ButtonsType.OK,
                "Importing settings from your ~/.jackdrc");
                d.run ();
                d.destroy ();
            } else {
                return false;
            }
        }
        
        string [] argv;
        cmdline = config.to_command_line();
        debug ("Starting jack with command line: %s\n", cmdline);
        Shell.parse_argv (cmdline, out argv);
        
        for (int i = 0; i < argv.length; i++) {
            debug ("argument: %d: %s\n", i, argv[i]);
        }
        
        bool jack_started = Process.spawn_async_with_pipes (null, argv, null, SpawnFlags.DO_NOT_REAP_CHILD, null, out jackd_pid, null, out jack_stdout, out jack_stderr);
        if (jack_started) {
            ChildWatch.add ((Pid)jackd_pid, (pid, status) => {
                debug ("Childwatch: PID: %d, exit-status: %d", (int)pid, status);
                reset_jack_output_sources ();
                
                if (client != null) {
                    client.deactivate ();
                    client = null;
                }
                
                jackd_pid = (Pid)0;
                initialized = false;
                post_jack_init ();
            });
            
            jack_output = new IOChannel.unix_new (jack_stdout);
            jack_output.set_close_on_unref (true);

            jack_output_source = jack_output.add_watch (IOCondition.IN | IOCondition.PRI, (source, condition) => {
                return get_channel_output (source, condition, jack_out);
            });
            
            Source.remove (output_timeout);
            output_timeout = Timeout.add (1000, () => { return notify_user (jack_out); });

            jack_errors = new IOChannel.unix_new (jack_stderr);
            jack_errors.set_close_on_unref (true);

            jack_errors_source = jack_errors.add_watch (IOCondition.IN | IOCondition.PRI, (source, condition) => {
                return get_channel_output (source, condition, jack_err); 
            });
            
            Source.remove (error_timeout);
            error_timeout = Timeout.add (1000, () => { return notify_user (jack_err); });
            
            return true;
        } else {
            return false;
        } 
    }
    
    protected bool get_channel_output (IOChannel source, IOCondition condition, StringBuilder builder) {
        debug ("IOCondition: %d", condition);
        string result;
        size_t length, terminator;
        if (0 != condition & (IOCondition.IN | IOCondition.PRI) ) {
            debug ("Data availble from JACK stdout, reading...");
            int retval = source.read_line (out result, out length, out terminator);
            debug ("read returned: %d, %s", retval, result);
            if (retval == IOStatus.NORMAL) {
                weak Mutex mutex = builder == jack_out ? jack_out_lock : jack_err_lock;
                
                mutex.lock ();
                builder.append (result);
                debug ("appended to builder %u: %s", (uint)builder, result);
                mutex.unlock ();
                return true;
            }
        }
        return false;
    }
    
    protected bool notify_user (StringBuilder builder) {
        weak Mutex mutex = builder == jack_out ? jack_out_lock : jack_err_lock;
        long len;
        string str;
        
        mutex.lock ();
        len = builder.len;
        str = builder.str;
        if (len != 0) builder.truncate (0);
        mutex.unlock ();
        
        if (len == 0) return true;
        
        debug ("notify_user called: len: %ld, str: %s", len, str);
        Notify.init (app_name);
        Notify.Notification n = new Notify.Notification ("JACK Output", str, builder == jack_out ? "gtk-dialog-info" : "gtk-dialog-error", null);
        n.set_timeout (10000);
        n.show ();
        return true;
    }
    
    public void start_jack () {
        debug ("start_jack called from Thread: %u", (uint)Thread.self ());
        if (initialized && (client != null)) {
            jack_err_lock.lock();
            jack_err.append ("\nJack has already started!\n");
            jack_err_lock.unlock ();
        }
    
        if (!start_jack_client ()) {
            if (start_jack_server ()) {
                Thread.usleep (2000000);
                // try twice to connect
                if (!start_jack_client ()) {
                    Thread.usleep (3000000);
                    
                    if (!start_jack_client ()) {
                        initialized = false;
                        return ;
                    }
                } 
            } else {
                initialized = false;
                return ;
            }
        }
        
        if ((status & Jack.Status.ServerStarted) != 0) {
            debug ("JACK server started\n");
        }
        
        if ((status & Jack.Status.NameNotUnique) != 0) {
			debug ("before client.get_name ()");
            client_name = client.get_name();
            debug ("unique name `%s' assigned\n", client_name);
        }
        
        client.on_shutdown (() => {
            debug ("CLIENT: ON_SHUTDOWN");
            client.deactivate ();
            client = null;
            reset_jack_output_sources ();
            initialized = false;
        });
        
        no_xruns = 0;
        client.set_xrun_callback (jackpanel_xrun_callback);

        if (client.activate () != 0) {
            error ("cannot activate client");
            initialized = false;
            return ;
        }
        
        initialized = true;
    }
    
    public signal void post_jack_init ();
    
    public bool init_error_dialog () {
        var d = new Gtk.MessageDialog (null, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE,
        "Could not start or connect to JACK\n Please check your settings");
        d.run ();
        d.destroy ();
        return false;
    }
    
        
    public void show_about_dialog () {
        var about = new Gtk.AboutDialog ();
        about.set_authors ({"Hans Baier"});
        about.set_copyright ("© 2009 Hans Baier");
        about.set_logo_icon_name ("jackpanel");
        about.set_website ("http://www.hans-baier.de/wordpress/jackpanel");
        about.set_license ("GPLv2 or later: \n http://www.gnu.org/licenses/gpl-2.0.html" + 
         "\n http://www.gnu.org/licenses/gpl.html");
        about.run ();
        about.destroy ();
    }
    
    public void show_configuration_dialog () {
        if (ConfigurationDialog.show_configuration_dialog ()){
            if (config.start_jack) {
                this.restart_jack ();
            } else {
                stop_jack ();
            }
        }
    }
   
   protected abstract void on_transport_stopped ();
   protected abstract void on_transport_rolling ();
   
   protected void show_time (PanelDisplayBase display) {
        Position current;// = Position ();
        TransportState transport_state;
        NFrames frame_time;

        //debug ("show_time");
        transport_state = client.transport_query (out current);
        frame_time = client.frame_time ();
    
        uint32 time_seconds = (uint32)current.frame / (uint32)current.frame_rate;
        time_h_min_sec   = "%02u:%02u:%02u".printf (time_seconds / 3600, (time_seconds % 3600) / 60, time_seconds % 60);
        time_in_frames   = "%u".printf (current.frame);

        switch (transport_state) {
        case TransportState.Stopped:
            on_transport_stopped ();
            break;
        case TransportState.Rolling:
            on_transport_rolling ();
            break;
        case TransportState.Starting:
            //debug ("state: Starting");
            break;
        default:
            critical ("jack transport state: [unknown]");
            break;
        }
        
        if ((current.valid & PositionBits.PositionBBT) != 0) {
            time_in_bbt = "%3u|%u|%04u".printf (current.bar, current.beat, current.tick);
            display.time_alt = time_in_bbt;
        } else {
            display.time_alt = time_in_frames;
        }
        
        display.time = time_h_min_sec;
    }
}

} //namespace Jackpanel

