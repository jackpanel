/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using Gtk;
using Gdk;
using Prolooks;

namespace Jackpanel {

public class Display : PanelDisplayBase {
    protected Menu  contextmenu;
    public signal void preferences ();
 
    private Jack.Client? make_vala_include_jack_h_workaround;
    
    construct {
        _width  = 182;
        _height = 49;
        set_size_request (width, height);
        contextmenu = new Menu ();
        var item = new MenuItem.with_label ("Preferences");
        item.activate += (item) => {
            preferences ();
        };
        
        contextmenu.append (item);
    }
    
    public bool jack_running { get; set; }
    
    private const double padding           = 3;
    private const double font_size         = 16;
    private       double vline_x;

    protected override void redraw_time () {
        queue_draw_area ((int)padding + (int)inner_x, (int)padding + (int)inner_y, (int) vline_x  + (int)padding - (int)inner_x, (int)font_size);
    }
    
    protected override void redraw_time_alt () {
        queue_draw_area ((int)vline_x + 2*(int)padding, (int)inner_y + (int)padding, (int)inner_width - (int)vline_x, (int)font_size); 
    }
    
    protected override bool draw_contents (Cairo.Context cr, Gdk.EventExpose event) {        
        double status_text_font_size = font_size - 4;
        double x                     = 0;
        double y                     = font_size;
        ///////////////////////////// ABOVE
        // show time text
        text (cr, time, padding + x, y, font_size);
        
        Cairo.TextExtents ext = Cairo.TextExtents();
        cr.text_extents (time_alt, out ext);
        
        // show alt text right justified
        text (cr, time_alt, inner_width - ext.x_advance - ext.x_bearing - padding, y, font_size);
        
        ////////////////////////////// LINES
        // draw lines
        Color line_color;
        Color.parse (text_color_default, out line_color);
        set_source_color (cr, line_color, 0.5);
        
        cr.text_extents (time, out ext);        
        // clock time separator line
        vline_x = padding + x + ext.x_advance + ext.x_bearing + padding + 1;
        
        cr.move_to (vline_x, padding);
        cr.rel_line_to (0, inner_height - 2 * padding + 1);
        cr.stroke();
        
        var hline_y = padding + ext.height + padding + 2; 
        
        // vertical separator line
        cr.move_to (padding, hline_y);
        cr.rel_line_to (width - 20, 0);
        cr.stroke();
        
        ////////////////////////////// BELOW
        var y_status_text = 0.5 + hline_y + status_text_font_size;
        
        // Realtime
        string rt = "RT";
        cr.text_extents (rt, out ext);
        if (realtime) {
            text (cr, rt, padding, y_status_text, status_text_font_size);
        }
        
        realtime_x = padding;
        realtime_y = y_status_text - ext.height;
        realtime_w = ext.x_advance;
        realtime_h = ext.height;
        
        var x_after_rt = padding + ext.x_advance;
        
        // Xruns
        string xruns_str = "X: %u".printf (xruns);
        cr.text_extents (xruns_str, out ext);
        if (xruns == 0) {
            text (cr, xruns_str, x_after_rt, y_status_text, status_text_font_size);
        } else {
            text (cr, xruns_str, x_after_rt, y_status_text, status_text_font_size, "#ff0000");
        }
        
        xruns_x = x_after_rt;
        xruns_y = y_status_text - ext.height;
        xruns_w = vline_x - x_after_rt;
        xruns_h = ext.height;
        
        // buffer size
        string buffer_size_str = "%2.1fms".printf (buffer_size * 1000.0 / (double)sample_rate);
        
        cr.text_extents (buffer_size_str, out ext);
        var y_popup_triangle = y_status_text - ext.height + 1.0;
        
        buffer_size_x = vline_x + padding;
        buffer_size_y = y_popup_triangle;
        buffer_size_w = ext.x_advance + ext.x_bearing + popup_triangle_width ();
        buffer_size_h = ext.height;
        text (cr, buffer_size_str, buffer_size_x, y_status_text, status_text_font_size);
        draw_popup_triangle (cr, buffer_size_x + ext.x_advance + ext.x_bearing + 1.0, y_popup_triangle);
        
        // sample_rate
        string sample_rate_str = "%u".printf (sample_rate);
        cr.text_extents (sample_rate_str, out ext);        
        
        sample_rate_x = inner_width - ext.x_advance - ext.x_bearing - padding - 1 - popup_triangle_width ();
        sample_rate_y = y_popup_triangle;
        sample_rate_w = ext.x_advance + ext.x_bearing + popup_triangle_width ();
        sample_rate_h = ext.height;
        text (cr, sample_rate_str, sample_rate_x, y_status_text, status_text_font_size);
        draw_popup_triangle (cr, inner_width - popup_triangle_width () - padding, y_popup_triangle);
                
        return false;
    }
    
    public override bool button_press_event (Gdk.EventButton event) {
       if (jack_running && event.type == EventType.BUTTON_PRESS && event.button == 1) {
           if (is_in_rect (event.x, event.y, xruns_x, xruns_y, xruns_w, xruns_h)) {
                reset_xruns_requested ();
                return true;
            }
        }
        if (jack_running && handle_events_for_active_items (event)) {
            return true;
        } else if (event.type == EventType.BUTTON_PRESS && event.button == 3) {
            contextmenu.popup (null, null, null, 3, event.time);
            contextmenu.show_all();
            return true;
        }
        
        return false;
    }
    
    private bool was_in_realtime_rect;
    
    public override bool motion_notify_event (Gdk.EventMotion event) {
    /*  This should highlight the sensitive areas the mouse is over
     *  but isn't quite ready yet
        var cr = Gdk.cairo_create (this.window);
        
        if (is_in_rect (event.x, event.y, realtime_x, realtime_y, realtime_w, realtime_h)) {
            was_in_realtime_rect = true;
            set_source_color_string (cr, text_color_default, 0.1);
            cr.rectangle (realtime_x, realtime_y, realtime_w, realtime_h);
            cr.fill ();
        } else {
            if (was_in_realtime_rect) {
                queue_draw ();
            }
        }
    */
        return false;
    }
    
}

} //namespace Jackpanel

