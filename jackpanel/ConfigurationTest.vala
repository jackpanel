/* 
    Copyright 2009 by Hans Baier
    License: GPLv2+ 
*/

using GLib;

namespace Jackpanel {

public class ConfigurationTest {
    protected static Configuration config;

    public static void set_up () {
        config = new Configuration ();
        config.set_test_mode ();
        config.init ();
    }

    public static void empty_test () {
        set_up ();
        assert (!config.exists ());
        config.sweep ();
        assert (!config.exists ());
        config.init ();
        assert (!config.exists ());
        config.sweep ();
        assert (!config.exists ());
    }
    
    public static void parse_command_line () {
        set_up ();
        assert (!config.exists ());
        string cmd = "/usr/bin/jackd -n my_name --bad-option1 -R -P79 -p 512 -t1000 -dalsa -r 48000 -p128 -n2 -D -C multi_capture -Pmulti_playback --bad-option2 -Xseq -i123 -o456";
        config.from_command_line (cmd);
        assert (config.exists ());
        assert (config.server_command == "/usr/bin/jackd");
        assert (config.realtime == true);
        assert (config.realtime_priority == 79);
        assert (config.driver == "alsa");
        assert (config.sample_rate == 48000);
        assert (config.buffer_size == 128);
        assert (config.periods == 2);
        assert (config.input_device == "multi_capture");
        assert (config.output_device == "multi_playback");
        assert (config.midi_driver == "seq");
        
        string unparsed = config.to_command_line ();
        string expected = "/usr/bin/jackd  --name my_name --realtime --realtime-priority 79 --port-max 512 --timeout 1000  --bad-option1 --driver alsa --rate 48000 --period 128 --nperiods 2 --inchannels 123 --outchannels 456 --capture multi_capture --playback multi_playback --midi seq  -D --bad-option2";
        debug ("unparsed: '%s'", unparsed);
        debug ("expected: '%s'", expected);
        assert (unparsed == expected);
        debug  ("jack options: '%s'",   config.jack_options);
        debug  ("driver options: '%s'", config.driver_options);
        assert (config.jack_options   == "--bad-option1");
        assert (config.driver_options == "-D --bad-option2");
        
        config.from_command_line (unparsed);
        assert (config.exists ());
        assert (config.server_command == "/usr/bin/jackd");
        assert (config.realtime == true);
        assert (config.realtime_priority == 79);
        assert (config.driver == "alsa");
        assert (config.sample_rate == 48000);
        assert (config.buffer_size == 128);
        assert (config.periods == 2);
        assert (config.input_device == "multi_capture");
        assert (config.output_device == "multi_playback");
        assert (config.midi_driver == "seq");
        unparsed = config.to_command_line ();
        debug ("unparsing got: '%s'", unparsed);
        debug ("expected: '%s'", expected);
        assert (unparsed == expected);
    }
    
     public static void make_sure_int_props_are_unparsed_correctly () {
            set_up ();
            assert (!config.exists ());
            string cmd = "/usr/bin/jackdddd  --name my_name";
            config.from_command_line (cmd);
            assert (config.exists ());
            assert (config.server_command == "/usr/bin/jackdddd");
            assert (config.server_name == "my_name");
            string unparsed = config.to_command_line ();
            debug ("unparsed: '%s'", unparsed);
            debug ("expected: '%s'", cmd);
            assert (unparsed == cmd);
        }
    
    static int main (string[] args) {
        Test.init (ref args);
        Test.add_func ("/jackpanel/configuration/empty", empty_test);
        Test.add_func ("/jackpanel/configuration/parse_command_line", parse_command_line);
        Test.add_func ("/jackpanel/configuration/make_sure_int_props_are_unparsed_correctly", make_sure_int_props_are_unparsed_correctly);
        Test.run ();
        
        // clean up
        Process.spawn_command_line_sync ("gconftool --shutdown");
        string test_dir = Environment.get_home_dir () + "/.gconf" + config.root ();
        Process.spawn_command_line_sync ("rm -rf " + test_dir);
        return 0;
    }
}

} //namespace Jackpanel

