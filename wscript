#!/usr/bin/env python
import os
import Options
import autowaf

VERSION = "0.1.1"
VERSION_MAJOR_MINOR = ".".join(VERSION.split(".")[0:2])
APPNAME = "jackpanel"
APPNAME_APPLET = "jackpanel-applet"

srcdir = '.'
blddir = 'build'

def set_options(opt):
    autowaf.set_options(opt)
    opt.tool_options('compiler_cc')

def configure(conf):
    autowaf.configure(conf)
    conf.check_tool('compiler_cc cc')
    conf.check_tool('vala')
    
    min_vala_version = (0, 7, 9)
    if conf.env['VALAC_VERSION'] < min_vala_version:
        conf.fatal('Your vala compiler version ' + str(conf.env['VALAC_VERSION']) +
                   ' is too old. The project requires at least version %d.%d.%d' % min_vala_version );

    autowaf.check_pkg(conf, 'jack',               uselib_store='JACK', atleast_version='0.116.1', mandatory=True)
    autowaf.check_pkg(conf, 'libpanelapplet-2.0', uselib_store='PANEL_APPLET', atleast_version='2.20.3', mandatory=False)
    autowaf.check_pkg(conf, 'libgnomeui-2.0',     uselib_store='LIBGNOMEUI',atleast_version='2.22.1', mandatory=False)
    autowaf.check_pkg(conf, 'cairo',              uselib_store='CAIRO', atleast_version='1.6.0', mandatory=True)
    autowaf.check_pkg(conf, 'gconf-2.0',          uselib_store='GCONF', atleast_version='2.22.0', mandatory=True)
    autowaf.check_pkg(conf, 'prolooks1',          uselib_store='PROLOOKS1', atleast_version='1.2.0', mandatory=True)
    autowaf.check_pkg(conf, 'hal',                uselib_store='HAL', atleast_version='0.5.11', mandatory=True)
    autowaf.check_pkg(conf, 'dbus-1',             uselib_store='DBUS', atleast_version='1.1.20', mandatory=True)
    autowaf.check_pkg(conf, 'dbus-glib-1',        uselib_store='DBUS_GLIB', atleast_version='0.74', mandatory=True)
    autowaf.check_pkg(conf, 'libnotify',          uselib_store='NOTIFY', atleast_version='0.4.4', mandatory=True)

    conf.env['CCFLAGS'] = '-I./default'

    conf.define('PACKAGE', APPNAME)
    conf.define('PACKAGE_NAME', APPNAME)
    conf.define('PACKAGE_STRING', APPNAME + '-' + VERSION)
    conf.define('PACKAGE_VERSION', APPNAME + '-' + VERSION)

    conf.define('VERSION', VERSION)
    conf.define('VERSION_MAJOR_MINOR', VERSION_MAJOR_MINOR)
    conf.define('APPNAME', APPNAME)
    conf.define('APPNAME_APPLET', APPNAME_APPLET)
    
    autowaf.print_summary(conf)
    opts = Options.options
    autowaf.display_header('Jackpanel Configuration')
    autowaf.display_msg(conf, 'Options:', str(opts))
    autowaf.display_msg(conf, 'Build GNOME panel applet', bool(conf.env['HAVE_PANEL_APPLET']))

def build(bld):
    bld.add_subdirs('jackpanel')
    
    # 'Desktop' file (menu entry, icon, etc)
    obj = bld.new_task_gen('subst')
    obj.source = 'jackpanel.desktop.in'
    obj.target = 'jackpanel.desktop'
    obj.dict = {
        'BINDIR'           : os.path.normpath(bld.env['BINDIR']),
        'APP_INSTALL_NAME' : APPNAME,
        'APP_HUMAN_NAME'   : APPNAME,
    }
    obj.install_path = '${DATADIR}applications'
    
    # Panel Applet .server file
    if bld.env['HAVE_PANEL_APPLET'] and bld.env['HAVE_LIBGNOMEUI']:
        obj = bld.new_task_gen('subst')
        obj.source = 'JackpanelApplet.server.in'
        obj.target = 'JackpanelApplet.server'
        obj.dict = {
            'BINDIR'           : os.path.normpath(bld.env['BINDIR']),
            'LIBDIR'           : os.path.normpath(bld.env['LIBDIR']),
            'APP_INSTALL_NAME' : APPNAME_APPLET,
            'APP_HUMAN_NAME'   : "Jackpanel Applet",
        }
        obj.install_path = '${LIBDIR}bonobo/servers'
    
    # icon cache is updated using:
    # gtk-update-icon-cache -f -t $(datadir)/icons/hicolor
    icon_sizes = ['16x16', '22x22', '24x24', '32x32', '48x48']
    for s in icon_sizes:
        bld.install_as(
            os.path.normpath(bld.env['DATADIR'] + '/icons/hicolor/' + s + '/apps/'
                + APPNAME + '.png'),
            'icons/' + s + '/jackpanel.png')
            
    bld.install_as(os.path.normpath(bld.env['DATADIR'] + '/pixmaps/' + APPNAME + '.xpm'),
                   'icons/16x16/jackpanel.xpm')

def shutdown():
    autowaf.shutdown()


